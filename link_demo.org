# -*- mode: org; coding: utf-8; -*-

* Header Information
#+TITLE:     GNU Emacs Org mode Link Demo: org-super-links + org-edna
#+AUTHOR:    Karl Voit
#+EMAIL:     tools@Karl-Voit.at
#+DATE:      2022-12-03
#+DESCRIPTION: A demo for showing aspects of knowledge-, todo- and project management with links between headings and defining dependencies.
#+KEYWORDS: emacs, orgmode, demo, PIM, dependencies, projects
#+LANGUAGE:  en

Explaining the visibility when opening the file:
#+STARTUP: overview   (or: showall, content, showeverything)
[[http://orgmode.org/org.html#Visibility-cycling]]  [[info:org#Visibility%20cycling][info:org#Visibility cycling]]

Defining the todo keywords:
#+TODO: TODO(t) NEXT(n) STARTED(s) WAITING(w@/!) SOMEDAY(S!) | DONE(d!/!) CANCELLED(c@/!)
[[http://orgmode.org/org.html#Per_002dfile-keywords]]  [[info:org#Per-file%20keywords][info:org#Per-file keywords]]

Defining some tags to play with:
#+TAGS: important(i) private(p)
#+TAGS: @HOME(h) @OFFICE(o)
[[http://orgmode.org/org.html#Setting-tags]]  [[info:org#Setting%20tags][info:org#Setting tags]]

* Setting Up the Demo

Disclaimer: This is a *dirty* demo setup process mainly because the
author does not understand how to do this in a clean way. Main issue
is the inability to make use-package run with quelpa to install
packages directly from GitHub. Therefore, some Elisp files are
downloaded and evaluated. Please do check them upfront if you're
unsure about evaluating whole files from third parties.

If you do have improvements for this self-contained setup, please do
hand in pull requests for [[https://gitlab.com/publicvoit/orgmode-link-demo][the public Git repository]].

1. [ ] optional for demo purposes: start *key visualization* tool like
   [[http://pabloseminario.com/projects/screenkey/][screenkey]].

2. [ ] optional for demo purposes: start *webcam* preview window with,
   e.g.,
   : mpv av://v4l2:/dev/video0 --profile=low-latency --untimed --title=Karl

3. [ ] *start emacs* with this file like: =emacs -q link_demo.org=

4. [ ] optional for demo purposes: adjust font size
   - de/increase by ~C-Mousewheel~

5. [ ] *execute babel block* for generic configuration by placing your
   cursor in the SRC block below and press =C-c C-c= and confirm with
   "yes" + =RET=

#+BEGIN_SRC emacs-lisp
;; parts of the Emacs setup is copied from: https://gitlab.com/skybert/my-little-friends/-/blob/master/emacs/.emacs

;;   (toggle-debug-on-error);; in case you need to identify an error here


(defun my-message(string)
  "Print out a message with mild formatting"
  (interactive)
  (message (concat "————→ " string " …"))
  )
;; (my-message "")
(my-message "START of demo config setup")

(my-message "turn off backup files")
(setq-default backup-inhibited t)
 
(my-message "set start of week to Monday (not Sunday) as I prefer and it should be according to ISO: https://en.wikipedia.org/wiki/ISO_week_date")
(setq-default calendar-week-start-day 1)

(my-message "set encoding")
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

(my-message "creating ephemeral configuration directories including ELPA storage")
(make-directory "configuration/elpa" "also create parents")
(my-message "setq user-emacs-directory to current directory")
(setq user-emacs-directory "configuration")
(my-message "setq package-user-dir as sub-directory")
(setq package-user-dir (concat default-directory "configuration/elpa")) ;; local demo elpa tree

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(my-message "Emacs package management")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(my-message "Initialise the emacs packages in case any of them overrides built-in Emacs packages")
(package-initialize)

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(setq package-archives '(("melpa" . "http://melpa.org/packages/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         )
      gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"
      )

(my-message "Ensure use-package is installed and loaded")
(condition-case nil
    (require 'use-package)
  (file-error
   (my-message "require 'package")
   (require 'package)
   (my-message "package-initialize")
   (package-initialize)
   (my-message "package-refresh-contents")
   (package-refresh-contents)
   (my-message "package-install 'use-package")
   (package-install 'use-package)
   (my-message "require 'use-package")
   (require 'use-package)))

(my-message "setq use-package-always-ensure: Set to true to have use-package install all packages mentioned if they're not already installed.")
(setq use-package-always-ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(my-message "installing packages for the demo")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(my-message "use-package org-edna: for defining dependencies: https://www.nongnu.org/org-edna-el/")
(use-package org-edna
  :config
  (require 'org-edna)
  (org-edna-load)
)

(my-message "use-package helm: completion and selection narrowing framework from https://emacs-helm.github.io/helm/")
(use-package helm)

(my-message "use-package helm-org: https://github.com/emacs-helm/helm-org")
(use-package helm-org)

(my-message "setting up peg (via downloading file and evaluating) which is used for org-ql")
(with-temp-buffer
  (url-insert-file-contents "http://git.savannah.gnu.org/gitweb/?p=emacs/elpa.git;a=blob_plain;f=peg.el;hb=refs/heads/externals/peg")
  (eval-buffer))

(my-message "use-package org-ql: An Org-mode query language, including search commands and saved views https://github.com/alphapapa/org-ql")
(use-package org-ql
  :config
  (require 'org-ql-search) ;; workaround for https://github.com/alphapapa/org-ql/issues/53
)
(my-message "setting up helm-org-ql (via downloading file and evaluating)")
(with-temp-buffer
  (url-insert-file-contents "https://github.com/alphapapa/org-ql/raw/master/helm-org-ql.el")
  (eval-buffer))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(my-message "General Org-mode settings")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'org)
(setq org-startup-indented t) ;; personal preference
(setq org-startup-folded 'overview) ;; personal preference

;; setup personal logging preferences:
(setq org-log-into-drawer t)
(setq org-log-redeadline nil)
(setq org-log-reschedule nil)

(setq org-expiry-inactive-timestamps t) ;; personal preference
(setq org-special-ctrl-a/e t) ;; convenient behavior for C-a and C-e


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(my-message "Demo-specific functions and settings")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; FIXXME: I could not find out how to install or load org-expiry here although it is part of org-contrib:
(my-message "org-expiry (author uses it for auto-logging to drawers)")
(with-temp-buffer
  (url-insert-file-contents "https://git.sr.ht/~bzg/org-contrib/blob/master/lisp/org-expiry.el")
  (eval-buffer))
(require 'org-expiry)
(my-message "Automatically write =CREATED= properties in the =PROPERTIES= drawer")
(org-expiry-insinuate)


(my-message "org-super-links (via downloading file and evaluating)")
(with-temp-buffer
  (url-insert-file-contents "https://github.com/toshism/org-super-links/raw/develop/org-super-links.el")
  (eval-buffer))
(with-temp-buffer
  (url-insert-file-contents "https://github.com/toshism/org-super-links/raw/develop/org-super-links-org-ql.el")
  (eval-buffer))
(with-temp-buffer
  (url-insert-file-contents "https://github.com/toshism/org-super-links/raw/develop/org-super-links-org-rifle.el")
  (eval-buffer))

;; the most important commands and bindings to insert links:
(define-key org-mode-map (kbd "C-c s i") 'org-super-links-quick-insert-inline-link)
(define-key org-mode-map (kbd "C-c s d") 'org-super-links-quick-insert-drawer-link)
;(bind-key (kbd "C-c s l") #'org-super-links-store-link)
;(bind-key (kbd "C-c s C-l") #'org-super-links-insert-link)
;(bind-key (kbd "C-c s s") #'org-super-links-link)



(setq org-super-links-related-into-drawer t
	org-super-links-link-prefix 'org-super-links-link-prefix-timestamp)

;; use my custom function to generate ID properties accordin to https://karl-voit.at/2019/11/16/UOMF-Linking-Headings/
(add-hook 'org-super-links-pre-link-hook 'my-id-get-or-generate)
(add-hook 'org-super-links-pre-backlink-hook 'my-id-get-or-generate)

;; Formats link descriptions as suggested on: https://github.com/toshism/org-super-links#limit-length-of-link-description
;; development: id:2020-08-05-implement-filter-function
(defun my-org-super-links-filter-description (link desc)
    ;; replace double bracket links with their description
    (replace-regexp-in-string org-link-bracket-re "\\2"
        ;; removes: <2020-08-04 Tue>--<2020-08-04 Tue 23:37>  (2nd time/date-stamp is optional; including inactive variants)
        (replace-regexp-in-string org-element--timestamp-regexp ""
              ;; removes priority indicators such as [#A]
	      (replace-regexp-in-string org-priority-regexp ""
                  ;; removes statistic cookies with absolute numbers such as [2/5]
       	          (replace-regexp-in-string " ?\\[[0-9]+/[0-9]+\\]" ""
                          ;; removes statistic cookies with percentages such as [33%]
         	          (replace-regexp-in-string " ?\\[[0-9]+%\\]" "" desc)
			      ))))
)
(setq org-super-links-default-description-formatter 'my-org-super-links-filter-description)
(setq org-super-links-backlink-into-drawer "LINKS"
      org-super-links-related-into-drawer "LINKS") ;; according to https://github.com/toshism/org-super-links/issues/33#issuecomment-678815869


(my-message "bind-key which defines =my-map= with =C-c C-,= (author's preference)")
(global-unset-key (kbd "C-c C-,"))
(use-package bind-key
  :bind (:prefix-map my-map
         :prefix-docstring "My own keyboard map"
         :prefix "C-c C-,"
         ("-" . text-scale-decrease)
         ("+" . text-scale-increase)
         ("=" . text-scale-increase));; because "+" needs "S-=" and I might forget to press shift
  :after org)
;; FIXXME: unfortunately, binding =C-c C-,= to my-map doesn't work here. Please send in pull requests with fixes.

(my-message "packages and functions for handling ID properties")

(use-package ffap)

(defun my-generate-sanitized-alnum-dash-string(str)
"Returns a string which contains only a-zA-Z0-9 with single dashes
 replacing all other characters in-between them.

 Some parts were copied and adapted from org-hugo-slug
 from https://github.com/kaushalmodi/ox-hugo (GPLv3)."
(let* (;; Remove "<FOO>..</FOO>" HTML tags if present.
       (str (replace-regexp-in-string "<\\(?1:[a-z]+\\)[^>]*>.*</\\1>" "" str))
       ;; Remove URLs if present in the string.  The ")" in the
       ;; below regexp is the closing parenthesis of a Markdown
       ;; link: [Desc](Link).
       (str (replace-regexp-in-string (concat "\\](" ffap-url-regexp "[^)]+)") "]" str))
       ;; Replace "&" with " and ", "." with " dot ", "+" with
       ;; " plus ".
       (str (replace-regexp-in-string
             "&" " and "
             (replace-regexp-in-string
              "\\." " dot "
              (replace-regexp-in-string
               "\\+" " plus " str))))
       ;; Replace German Umlauts with 7-bit ASCII.
       (str (replace-regexp-in-string "ä" "ae" str nil))
       (str (replace-regexp-in-string "ü" "ue" str nil))
       (str (replace-regexp-in-string "ö" "oe" str nil))
       (str (replace-regexp-in-string "ß" "ss" str nil))
       ;; Replace all characters except alphabets, numbers and
       ;; parentheses with spaces.
       (str (replace-regexp-in-string "[^[:alnum:]()]" " " str))
       ;; On emacs 24.5, multibyte punctuation characters like "："
       ;; are considered as alphanumeric characters! Below evals to
       ;; non-nil on emacs 24.5:
       ;;   (string-match-p "[[:alnum:]]+" "：")
       ;; So replace them with space manually..
       (str (if (version< emacs-version "25.0")
                (let ((multibyte-punctuations-str "：")) ;String of multibyte punctuation chars
                  (replace-regexp-in-string (format "[%s]" multibyte-punctuations-str) " " str))
              str))
       ;; Remove leading and trailing whitespace.
       (str (replace-regexp-in-string "\\(^[[:space:]]*\\|[[:space:]]*$\\)" "" str))
       ;; Replace 2 or more spaces with a single space.
       (str (replace-regexp-in-string "[[:space:]]\\{2,\\}" " " str))
       ;; Replace parentheses with double-hyphens.
       (str (replace-regexp-in-string "\\s-*([[:space:]]*\\([^)]+?\\)[[:space:]]*)\\s-*" " -\\1- " str))
       ;; Remove any remaining parentheses character.
       (str (replace-regexp-in-string "[()]" "" str))
       ;; Replace spaces with hyphens.
       (str (replace-regexp-in-string " " "-" str))
       ;; Remove leading and trailing hyphens.
       (str (replace-regexp-in-string "\\(^[-]*\\|[-]*$\\)" "" str)))
  str)
)

(defun my-id-get-or-generate()
"Returns the ID property if set or generates and returns a new one if not set.
 The generated ID is stripped off potential progress indicator cookies and
 sanitized to get a slug. Furthermore, it is prepended with an ISO date-stamp
 if none was found before."
    (interactive)
        (when (not (org-id-get))
            (progn
               (let* (
                      (my-heading-text (nth 4 (org-heading-components)));; retrieve heading string
                      (my-heading-text (replace-regexp-in-string "\\(\\[[0-9]+%\\]\\)" "" my-heading-text));; remove progress indicators like "[25%]"
                      (my-heading-text (replace-regexp-in-string "\\(\\[[0-9]+/[0-9]+\\]\\)" "" my-heading-text));; remove progress indicators like "[2/7]"
                      (my-heading-text (replace-regexp-in-string "\\(\\[#[ABC]\\]\\)" "" my-heading-text));; remove priority indicators like "[#A]"
                      (my-heading-text (replace-regexp-in-string "\\[\\[\\(.+?\\)\\]\\[" "" my-heading-text t));; removes links, keeps their description and ending brackets
                      (my-heading-text (replace-regexp-in-string "<[12][0-9]\\{3\\}-[0-9]\\{2\\}-[0-9]\\{2\\}\\( .*?\\)>" "" my-heading-text t));; removes day of week and time from active date- and time-stamps
                      (my-heading-text (replace-regexp-in-string "\\[[12][0-9]\\{3\\}-[0-9]\\{2\\}-[0-9]\\{2\\}\\( .*?\\)\\]" "" my-heading-text t));; removes day of week and time from inactive date- and time-stamps
                      (new-id (my-generate-sanitized-alnum-dash-string my-heading-text));; get slug from heading text
                      (my-created-property (assoc "CREATED" (org-entry-properties))) ;; nil or content of CREATED time-stamp
                     )
                   (when (not (string-match "[12][0-9][0-9][0-9]-[01][0-9]-[0123][0-9]-.+" new-id))
                           ;; only if no ISO date-stamp is found at the beginning of the new id:
                           (if my-created-property (progn
                               ;; prefer date-stamp of CREATED property (if found):
                               (setq my-created-datestamp (substring (org-entry-get nil "CREATED" nil) 1 11)) ;; returns "2021-12-16" or nil (if no CREATED property)
                               (setq new-id (concat my-created-datestamp "-" new-id))
                           )
                           ;; use today's date-stamp if no CREATED property is found:
                           (setq new-id (concat (format-time-string "%Y-%m-%d-") new-id))))
                   (org-set-property "ID" new-id)
                   )
                 )
        )
        (kill-new (concat "id:" (org-id-get)));; put ID in kill-ring
        (org-id-get);; retrieve the current ID in any case as return value
)

(my-message "=C-c C-,= I does get (or generate) an ID")
(bind-key (kbd "I") #'my-id-get-or-generate my-map)


(my-message "org-linker-*")

(unless (package-installed-p 'org-linker)
  (with-temp-buffer
    (url-insert-file-contents "https://raw.githubusercontent.com/toshism/org-linker/master/org-linker.el")
    (eval-buffer)
    ))


(unless (package-installed-p 'org-linker-edna)
  (with-temp-buffer
    (url-insert-file-contents "https://raw.githubusercontent.com/toshism/org-linker-edna/master/org-linker-edna.el")
    (eval-buffer)
    ))

(my-message "use my ID generation function instead of UUIDs: https://github.com/toshism/org-linker-edna/issues/3#issuecomment-703815638")
(setq link-id-function 'my-id-get-or-generate)
(my-message "=C-c s e= invokes org-linker-edna() to define dependencies")
(bind-key "C-c s e" #'org-linker-edna)


(my-message "END of demo config setup")
#+END_SRC

* Demo: Bi-Directional Links Between Two Headings

- Demo script

  1. Use =C-c s i= (org-super-links-quick-insert-inline-link) to link
     the text from the first heading to the second one.

  2. Look at the things that happened:
     1. =ID= properties for each heading
     2. =LINKS= drawer + content

  3. Use =C-c s d= (org-super-links-quick-insert-drawer-link) to link
     the third heading to the fourth.

  4. Look at the things that happened:
     1. =ID= properties for each heading
     2. =LINKS= drawer + content

** first heading

Let's link to 

** second heading

** third heading

** fourth heading

* Demo: Quickly Define a Small Project With a Finalization Task

- Demo script
  1. [ ] Brainstorm project and generate a simple list (already prepared).

  2. [ ] Convert list to headings via =C-c *= (org-ctrl-c-star)

  3. [ ] Add NEXT keyword to first task: "Empty garage".
     - Note that =org-expiry-insinuate= automatically creates
       =CREATED= properties.
     - Disclaimer: I currently don't have a good solution: a task
       requires two other tasks to be finished.

  4. [ ] Add dependency between "Empty garage" and "Paint walls and floor":
     - =C-c s e= (org-linker-edna)

  5. [ ] Notice the things that happened in the background for you:
     - human-readable =ID= properties for each heading
     - =TRIGGER= and =BLOCKER= properties via to org-edna
     - Navigate via the IDs mentioned in the =TRIGGER= and =BLOCKER=
       properties

  6. [ ] Add dependency between the rest of the tasks: each one marks
     the follow-up task with NEXT.

  7. [ ] Add dependency between the last task "Garage is painted and
     stuff is returned → close project" and "The Garage Project" to
     mark the overall project as DONE.

  8. [ ] Mark tasks as DONE one by one and look at its effects.


** [/] The Garage Project                                                                  :project:

- Why?
  - To get rid of the old stains and get a fresh feeling when being in my garage.
  - As discussed with my dear wife on [2022-10-29 Sat].

- Empty garage
- Paint walls and floor
- Bring back stuff into garage
- Garage is painted and stuff is returned → close project


